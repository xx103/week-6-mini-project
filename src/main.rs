use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use tracing::{error, info, Level};
use tracing_subscriber;
use tracing_subscriber::fmt::format::FmtSpan;
use tracing::instrument;


#[tokio::main]
async fn main() -> Result<(), Error> {
    // Initialize the subscriber for tracing
    tracing_subscriber::fmt()
        .json() // Output logs in JSON format for better structure and queryability
        .with_max_level(Level::INFO) // Set maximum log level to INFO to capture relevant logs
        .with_span_events(FmtSpan::CLOSE) // Log when spans are closed to trace execution flow
        .init();

    // Log the starting of the service with a structured message
    info!("Starting the lambda service");

    // Define the handler function using `service_fn`
    let func = service_fn(calculate_grade);

    // Before executing the lambda runtime, log an informational message
    info!("Running the lambda runtime");

    // Execute the lambda function within a match statement to capture and log errors
    match lambda_runtime::run(func).await {
        Ok(_) => info!("Lambda executed successfully"),
        Err(e) => {
            // Log any errors encountered during lambda execution with a structured error message
            error!("Lambda runtime failed to start: {:?}", e);
            // Return the error to indicate failure in the main function
            return Err(e);
        }
    }

    // If everything goes well, return Ok to indicate success
    Ok(())
}



#[derive(Debug, Deserialize)]
struct GradeInput {
    quiz1_grade: f32,
    quiz2_grade: f32,
    hw_grade: f32,
    exam_grade: f32,
    quiz_weight: f32,
    hw_weight: f32,
    exam_weight: f32,
}

#[derive(Serialize)]
struct GradeOutput {
    overall_grade: f32,
}



#[instrument]
async fn calculate_grade(event: LambdaEvent<Value>) -> Result<GradeOutput, Error> {
    let (value, _context) = event.into_parts();

    // Try to deserialize the Value into GradeInput
    let input: GradeInput = match serde_json::from_value(value) {
        Ok(input) => input,
        Err(e) => {
            error!("Failed to deserialize input: {:?}", e);
            return Err(Error::from(e.to_string()));
        }
    };

    info!("Calculating overall grade");
    info!("Received input: {:?}", input);
    
    let overall_grade = (input.quiz1_grade + input.quiz2_grade) * input.quiz_weight / 2.0
        + input.hw_grade * input.hw_weight
        + input.exam_grade * input.exam_weight;

    if overall_grade.is_nan() {
        error!("Failed to calculate overall grade: encountered NaN");
        return Err(Error::from("Calculation resulted in NaN"));
    }

    Ok(GradeOutput { overall_grade })
}


