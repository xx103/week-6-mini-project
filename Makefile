.PHONY: versions format install lint test watch build deploy aws-invoke all

versions:
	echo "Rust command-line utility versions:"
	rustc --version 			# Rust compiler
	cargo --version 			# Rust package manager
	rustfmt --version			# Rust code formatter
	rustup --version			# Rust toolchain manager
	clippy-driver --version	# Rust linter

format:
	cargo fmt --quiet

install:
	echo "Updating rust toolchain"
	rustup update stable
	rustup default stable 

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

watch:
	cargo watch -x check -x test

build: 
	cargo lambda build --release 

deploy:
	cargo lambda deploy 

aws-invoke:
	cargo lambda invoke --remote rust_lambda_grade_calculator --data-ascii '{ "quiz1_grade": 85.0, "quiz2_grade": 90.0, "hw_grade": 80.0, "exam_grade": 95.0, "quiz_weight": 0.25, "hw_weight": 0.25, "exam_weight": 0.5 }'

all: format lint test
