# Mini-project 6: Rust Lambda Logging and Tracing 

## Project Overview

This project demonstrates how to instrument a Rust Lambda function for logging and tracing, integrating it with AWS services like X-Ray and CloudWatch. The Lambda function calculates an overall grade based on input grades and weights for quizzes, homework, and exams.

## Setup Instructions

### Prerequisites

- Rust installed on your system.
- AWS CLI configured with necessary permissions (AWSLambda_FullAccess, IAMFullAccess, AWSXrayFullAccess).
- Cargo Lambda installed for Rust Lambda development.

### 1. IAM User Setup

- Log in to your AWS account, navigate to the IAM (Identity and Access Management) console, and create a new user
- Attach `AWSLambda_FullAccess`, `IAMFullAccess`, and `AWSXrayFullAccess` policies.
- Note down the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

### 2. Project Setup

- Create a new project with `cargo lambda new project_name`.
- Update `main.rs` and `Cargo.toml`.
- Add a `.env` file with your AWS credentials and region, and ensure it's listed in `.gitignore`.
- Export your AWS credentials and region in your terminal session.
- Run `cargo lambda watch` in the terminal to test your Lambda function
- Run `cargo lambda invoke  --data-ascii '{ "quiz1_grade": 85.0, "quiz2_grade": 90.0, "hw_grade": 80.0, "exam_grade": 95.0, "quiz_weight": 0.25, "hw_weight": 0.25, "exam_weight": 0.5 }'` to invoke your Lambda function locally

- Use the following commands to deploy your Lambda function:
        ```bash
        cargo lambda build --release
        cargo lambda deploy
        ```


### 3. AWS Lambda Setup

- Navigate to the AWS Management Console and open the Lambda service.
- Locate your deployed Lambda function in the dashboard and select it.
- Within the function's detail page, go to the "Configuration" tab.
- In Additional monitoring tools section, enable `X-Ray active tracing` and `Lambda Insights enhanced monitoring`
- Run `cargo lambda invoke --remote rust_lambda_grade_calculator --data-ascii '{ "quiz1_grade": 85.0, "quiz2_grade": 90.0, "hw_grade": 80.0, "exam_grade": 95.0, "quiz_weight": 0.25, "hw_weight": 0.25, "exam_weight": 0.5 }'` to test remote invoke.
- You can also test the Lambda function if you go to the "Test" tab, implement the Event Json, and click on `Test`.

### 4. API Gateway Setup
- From the AWS Management Console, access the API Gateway service.
- Initiate the creation of a new `REST API` within the API Gateway service.
- For your API, incorporate an `ANY` method.
- Once configured, proceed to deploy your API. You will be provided with an Invoke URL.
- Utilize the terminal to test the functionality of your API Gateway. 

## Lambda `rust_lambda_grade_calculator` function: 
![Function overview](/screenshots/screenshot1.png)

## test the API gateway using curl:
![Function overview](/screenshots/screenshot2.png)

## test Lambda function in AWS
![Function overview](/screenshots/screenshot3.png)
![Function overview](/screenshots/screenshot4.png)

## CloudWatch traces with remote invoke in local terminal and AWS
![Function overview](/screenshots/screenshot5.png)
![Function overview](/screenshots/screenshot6.png)


## Trace-level Loggings
![Function overview](/screenshots/screenshot7.png)
![Function overview](/screenshots/screenshot8.png)



